# axa-hero-ios

Third party libraries:


- Alamofire:
  The most HTTP networking library used for swift development and it was used in this project to avoid too many lines of code doing a request.

- ObjectMapper:
  Used to convert JSON to model objects easily.

- Kingfisher:
  A good option that was used to downloading and caching image.

- SVProgressHUD:
  It's a loader that was used to make things beautiful.
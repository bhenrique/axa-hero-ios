//
//  Brastlewark.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import Foundation
import ObjectMapper

struct Brastlewark: Mappable {

    var gnomes = [Gnome]()
    
    init() { }
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        gnomes <- map["Brastlewark"]
    }
}

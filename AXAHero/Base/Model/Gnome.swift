//
//  Gnome.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import Foundation
import ObjectMapper

struct Gnome: Mappable, Equatable {

    var id = 0
    var name = ""
    var thumbnail = ""
    var age = 0
    var weight = 0.0
    var height = 0.0
    var hair_color = ""
    var professions = [""]
    var friends = [""]
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        thumbnail <- map["thumbnail"]
        age <- map["age"]
        weight <- map["weight"]
        height <- map["height"]
        hair_color <- map["hair_color"]
        professions <- map["professions"]
        friends <- map["friends"]
    }
    
    static func ==(lhs: Gnome, rhs: Gnome) -> Bool {
        return lhs.name == rhs.name
    }
}

//
//  HomeViewModel.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import Foundation

class HomeViewModel {

    var town = Brastlewark()
    var selectedFilter: SelectedFilter?
    private var gnomesFromFilterSelection = [Gnome]()
    private var gnomesOrderedByName = [Gnome]()
    private var filteredGnomes = [Gnome]()
    private weak var delegate: LoadContent?
    
    init(delegate: LoadContent) {
        self.delegate = delegate
    }
    
    func loadContent() {
        BrastlewarkRequest().getTown { success, brastlewark in
            if let brastlewark = brastlewark {
                self.town = brastlewark
                self.gnomesOrderedByName = self.orderedByName()
                self.filteredGnomes = self.orderedByName()
                self.gnomesFromFilterSelection = self.orderedByName()
            }
            self.delegate?.didLoadContent(with: success)
        }
    }
    
    func orderedByName() -> [Gnome] {
        return town.gnomes.sorted(by: { $0.0.name < $0.1.name })
    }
    
    func getNumberOfRows(isSearching: Bool) -> Int {
        if let _ = selectedFilter {
            return filteredGnomes.count
        }
        return isSearching ? filteredGnomes.count : town.gnomes.count
    }
    
    func getNumberOfSections() -> Int {
        return 1
    }
    
    func filterByName(text: String) {
        if let _ = selectedFilter {
            filteredGnomes = text.isEmpty ? gnomesFromFilterSelection : gnomesFromFilterSelection.filter { $0.name.contains(text) }
        } else {
            filteredGnomes = gnomesOrderedByName.filter { $0.name.contains(text) }
        }
        delegate?.didLoadContent(with: true)
    }
    
    func applyFilter(selectedFilter: SelectedFilter?) {
        guard let selectedFilter = selectedFilter else {
            self.selectedFilter = nil
            self.gnomesFromFilterSelection = gnomesOrderedByName
            delegate?.didLoadContent(with: true)
            return
        }
        
        self.selectedFilter = selectedFilter
        
        if selectedFilter.ages.count > 0 {
            filterByAge(ages: selectedFilter.ages)
        }
        
        if selectedFilter.hairs.count > 0 {
            filterByHair(hairs: selectedFilter.hairs)
        }
        
        if selectedFilter.professions.count > 0 {
            filterByProfessions(professions: selectedFilter.professions)
        }
        
        if selectedFilter.friends.count > 0 {
            filterByFriends(friends: selectedFilter.friends)
        }
        
        gnomesFromFilterSelection = filteredGnomes
        delegate?.didLoadContent(with: true)
    }
    
    func dtoFromIndex(isSearching: Bool, index: Int) -> GnomeCellDTO {
        if let _ = selectedFilter, let gnome = filteredGnomes.object(index: index) {
            return GnomeCellDTO(imageUrl: gnome.thumbnail, name: gnome.name, age: gnome.age, id: gnome.id)
        }
        
        guard let gnome =  isSearching ? filteredGnomes.object(index: index) : orderedByName().object(index: index) else {
            return GnomeCellDTO()
        }
        return GnomeCellDTO(imageUrl: gnome.thumbnail, name: gnome.name, age: gnome.age, id: gnome.id)
    }
    
    func getDetailDTO(id: Int) -> DetailDTO {
        if let gnome = gnomesOrderedByName.filter({ $0.id == id }).first {
            return DetailDTO(structure: [[gnome.thumbnail, gnome.name], ["Age: \(gnome.age)", "Hair color: \(gnome.hair_color) ", "Height: \(gnome.height)", "Weight: \(gnome.weight)", "Id: \(gnome.id)"], gnome.professions, gnome.friends])
        }
        return DetailDTO()
    }
    
    // MARK: Private functions
    
    private func filterByAge(ages: [String]) {
        filteredGnomes = gnomesFromFilterSelection.filter { ages.contains("\($0.age)") }
    }
    
    private func filterByHair(hairs: [String]) {
        filteredGnomes = gnomesFromFilterSelection.filter { hairs.contains($0.hair_color) }
    }
    
    private func filterByProfessions(professions: [String]) {
        var gnomesFound = [Gnome]()
        gnomesFromFilterSelection.forEach { gnome in
            if gnome.professions.filter({ professions.contains($0) }).count > 0 {
                gnomesFound.append(gnome)
            }
        }
        filteredGnomes = gnomesFromFilterSelection.filter { gnomesFound.contains($0) }
    }
    
    private func filterByFriends(friends: [String]) {
        var gnomesFound = [Gnome]()
        gnomesFromFilterSelection.forEach { gnome in
            if gnome.friends.filter({ friends.contains($0) }).count > 0 {
                gnomesFound.append(gnome)
            }
        }
        
        if friends.contains("None") {
            gnomesFromFilterSelection.forEach { gnome in
                if gnome.friends.count == 0 {
                    gnomesFound.append(gnome)
                }
            }
        }
        
        filteredGnomes = gnomesFromFilterSelection.filter { gnomesFound.contains($0) }
    }
}

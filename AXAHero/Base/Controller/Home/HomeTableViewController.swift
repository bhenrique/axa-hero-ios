//
//  HomeTableViewController.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright � 2017 Bruno Santos. All rights reserved.
//

import UIKit

protocol LoadContent: class {
    func didLoadContent(with success: Bool)
}

protocol UpdateFilter: class {
    func didApplyFilter(selectedFilter: SelectedFilter?)
}

class HomeTableViewController: UITableViewController, UISearchBarDelegate, LoadContent, UpdateFilter {

    @IBOutlet weak var filterButton: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!
    private lazy var viewModel: HomeViewModel = HomeViewModel(delegate: self)
    private let pullToRefresh = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPullToRefresh()
        loadContent()
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = pullToRefresh
        } else {
            tableView.addSubview(pullToRefresh)
        }
        pullToRefresh.addTarget(self, action: #selector(self.loadContent), for: .valueChanged)
    }
    
    func loadContent() {
        startLoading()
        viewModel.loadContent()
    }
    
    // MARK: IBAction
    
    @IBAction func showFilterView() {
        performSegue(withIdentifier: "showFilterView", sender: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getNumberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows(isSearching: tableView != self.tableView)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GnomeCell = UITableViewCell.createCell(tableView: self.tableView, indexPath: indexPath)
        cell.fill(dto: viewModel.dtoFromIndex(isSearching: tableView != self.tableView, index: indexPath.row))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? GnomeCell {
            performSegue(withIdentifier: "showDetail", sender: viewModel.getDetailDTO(id: cell.id))
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
 
    // MARK: LoadContent
    
    func didLoadContent(with success: Bool) {
        stopLoading()
        if !success {
            showTryAgainAlert(message: "An error occured, please, try again.", completeBlock: { action in
                if action.title == "Try again" {
                    self.loadContent()
                }
                self.pullToRefresh.endRefreshing()
                self.filterButton.isEnabled = false
                self.searchBar.isUserInteractionEnabled = false
            })
        } else {
            DispatchQueue.main.async {
                self.filterButton.isEnabled = true
                self.searchBar.isUserInteractionEnabled = true
                self.pullToRefresh.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: UpdateFilter
    func didApplyFilter(selectedFilter: SelectedFilter?) {
        viewModel.applyFilter(selectedFilter: selectedFilter)
    }
    
    // MARK: - SearchBarDelegate

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filterByName(text: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, !text.isEmpty {
            viewModel.filterByName(text: text)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.filterByName(text: "")
    }
    
    // MARK: PrepareFroSege
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? FilterTableViewController {
            if let selectedFilter = self.viewModel.selectedFilter {
                destination.setSelectedFilter(selectedFilter: selectedFilter)
            }
            destination.setTown(town: self.viewModel.town)
            destination.delegate = self
        }
        if let destination = segue.destination as? DetailTableViewController, let dto = sender as? DetailDTO {
            destination.setDetailDTO(dto: dto)
        }
    }
}

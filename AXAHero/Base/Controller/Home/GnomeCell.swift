//
//  GnomeCell.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import UIKit
import Kingfisher

struct GnomeCellDTO {
    var imageUrl = ""
    var name = ""
    var age = 0
    var id = 0
}

class GnomeCell: UITableViewCell {

    @IBOutlet weak var gnomeImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    
    var id = 0
    
    func fill(dto: GnomeCellDTO) {
        gnomeImage.kf.setImage(with: URL(string: dto.imageUrl))
        nameLbl.text = dto.name
        ageLbl.text = "\(dto.age) years old"
        id = dto.id
    }
}

//
//  DetailTableViewController.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import UIKit

struct DetailDTO {
    var structure = [[""]]
}

class DetailTableViewController: UITableViewController {

    var viewModel = DetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.dto.structure.first?.last
    }
    
    func setDetailDTO(dto: DetailDTO) {
        viewModel.dto = dto
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsForSection(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell: PictureCell = UITableViewCell.createCell(tableView: tableView, indexPath: indexPath)
            cell.fill(dto: viewModel.pictureDTOForRow(row: indexPath.row))
            return cell
        }
        let cell: DetailCell = UITableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(dto: viewModel.detailDTOFor(section: indexPath.section, row: indexPath.row))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return nil
        }
        return viewModel.titleForHeader(section: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return 297
        }
        return 48
    }
}

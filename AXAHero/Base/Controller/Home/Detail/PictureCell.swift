//
//  PictureCell.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import UIKit
import Kingfisher

struct PictureCellDTO {
    var url = ""
}

class PictureCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    
    func fill(dto: PictureCellDTO) {
        picture.kf.setImage(with: URL(string: dto.url))
    }
}

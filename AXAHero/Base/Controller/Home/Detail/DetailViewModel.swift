//
//  DetailViewModel.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import Foundation

fileprivate enum Section: Int {
    case picture
    case detail
    case professions
    case friends
    case none
    
    var stringValue: String {
        switch self {
        case .picture:
            return "Picture"
        case .detail:
            return "Detail"
        case .professions:
            return "Professions"
        case .friends:
            return "Friends"
        case .none:
            return "Extra information"
        }
    }
}

class DetailViewModel {
    
    var dto = DetailDTO()

    init() {
        
    }
    
    func numberOfSections() -> Int {
        return dto.structure.filter({ $0.count > 0 }).count
    }
    
    func numberOfRowsForSection(section: Int) -> Int {
        return dto.structure.object(index: section)?.count ?? 0
    }
    
    func pictureDTOForRow(row: Int) -> PictureCellDTO {
        return PictureCellDTO(url: dto.structure.first?.object(index: row) ?? "")
    }
    
    func detailDTOFor(section: Int, row: Int) -> DetailCellDTO {
        return DetailCellDTO(title: dto.structure.object(index: section)?.object(index: row) ?? "")
    }
    
    func titleForHeader(section: Int) -> String? {
        guard let sectionEnum = Section(rawValue: section) else {
            return ""
        }
        return dto.structure.object(index: section)?.count ?? 0 > 0 ? sectionEnum.stringValue : nil
    }
}

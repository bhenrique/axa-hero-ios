//
//  DetailCell.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import UIKit

struct DetailCellDTO {
    var title = ""
}

class DetailCell: UITableViewCell {

    @IBOutlet weak var detailLbl: UILabel!
    
    func fill(dto: DetailCellDTO) {
        detailLbl.text = dto.title
    }

}

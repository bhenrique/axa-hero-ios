//
//  FilterTableViewController.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import UIKit

protocol FilterData: class {
    func didSetupFilterData()
}

class FilterTableViewController: UITableViewController, FilterData {

    @IBOutlet weak var clearButton: UIBarButtonItem!
    lazy var viewModel: FilterViewModel =  FilterViewModel(delegate: self)
    weak var delegate: UpdateFilter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.global().async {
            self.viewModel.createFilterData()
        }
    }
    
    func setTown(town: Brastlewark) {
        viewModel.setTown(town: town)
    }
    
    func setSelectedFilter(selectedFilter: SelectedFilter) {
        viewModel.setSelectedFilter(selectedFilter: selectedFilter)
    }
    
    @IBAction func clearAll() {
        viewModel.clearAll()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsForSection(section: section)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeaderInSection(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FilterCell = UITableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(dto: viewModel.dtoForRowAndSection(section: indexPath.section, row: indexPath.row))
        cell.accessoryType = viewModel.isSelected(title: cell.title) ? .checkmark : .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? FilterCell {
            cell.accessoryType == .none ? viewModel.selectFilter(section: indexPath.section, index: indexPath.row) : viewModel.deselectFilter(section: indexPath.section, title: cell.title)
        }
    }

    // MARK: FilterData
    func didSetupFilterData() {
        DispatchQueue.main.async {
            self.clearButton.isEnabled = self.viewModel.shouldShowClear()
            self.tableView.reloadData()
        }
    }
    
    @IBAction func applyFilter() {
        delegate?.didApplyFilter(selectedFilter: viewModel.getSelectedFilter())
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

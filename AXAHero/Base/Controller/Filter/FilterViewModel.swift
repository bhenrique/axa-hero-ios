//
//  FilterViewModel.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import Foundation

fileprivate enum Section: Int {
    case professions
    case hairs
    case friends
    case age
    
    var stringValue: String {
        switch self {
        case .professions:
            return "Profession"
        case .hairs:
            return "Hair Color"
        case .friends:
            return "Friends"
        case .age:
            return "Age"
        }
    }
}

typealias SelectedFilter = (professions: [String], friends: [String], hairs: [String], ages: [String])

class FilterViewModel {
    
    private var town = Brastlewark()
    private weak var delegate: FilterData?
    
    private var professions = [String]()
    private var friends = [String]()
    private var hairs = [String]()
    private var ages = [String]()
    
    private var selectedFilter = SelectedFilter(professions: [String](),
                                                friends: [String](),
                                                hairs: [String](),
                                                ages: [String]())
    
    init(delegate: FilterData) {
        self.delegate = delegate

    }
    
    func setTown(town: Brastlewark) {
        self.town = town
    }
    
    func setSelectedFilter(selectedFilter: SelectedFilter) {
        self.selectedFilter = selectedFilter
    }
    
    func createFilterData() {
        createProfessionsFilter()
        createHairColorFilter()
        createFriendsFilter()
        createAgeFilter()
        delegate?.didSetupFilterData()
    }
    
    func titleForHeaderInSection(section: Int) -> String {
        guard let section = Section(rawValue: section) else {
            return ""
        }
        return section.stringValue
    }
    
    func numberOfSections() -> Int {
        return 4
    }
    
    func numberOfRowsForSection(section: Int) -> Int {
        guard let section = Section(rawValue: section) else {
            return 0
        }
        
        switch section {
        case .professions:
            return professions.count
        case .hairs:
            return hairs.count
        case .friends:
            return friends.count + 1
        case .age:
            return ages.count
        }
    }
    
    func dtoForRowAndSection(section: Int, row: Int) -> FilterCellDTO {
        guard let title = elementsForSection(section: section).object(index: row) else {
            return FilterCellDTO()
        }
        
        if Section(rawValue: section) == .friends && row == 0 {
            return FilterCellDTO(title: "None")
        }
        
        return FilterCellDTO(title: title)
    }
    
    func selectFilter(section: Int, index: Int) {
        guard let section = Section(rawValue: section) else {
            return
        }
        
        switch section {
        case .age:
            if let ages = ages.object(index: index) {
                selectedFilter.ages.append(ages)
            }
        case .hairs:
            if let hair = hairs.object(index: index) {
                selectedFilter.hairs.append(hair)
            }
        case .friends:
            if index == 0 {
                selectedFilter.friends.append("None")
                break
            }
            if let friend = friends.object(index: index - 1) {
                selectedFilter.friends.append(friend)
            }
        case .professions:
            if let profession = professions.object(index: index) {
                selectedFilter.professions.append(profession)
            }
        }
        
        delegate?.didSetupFilterData()
    }
    
    func deselectFilter(section: Int, title: String?) {
        guard let section = Section(rawValue: section), let title = title else {
            return
        }
        
        switch section {
        case .age:
            selectedFilter.ages = selectedFilter.ages.filter { $0 != title }
        case .hairs:
            selectedFilter.hairs = selectedFilter.hairs.filter { $0 != title }
        case .friends:
            selectedFilter.friends = selectedFilter.friends.filter { $0 != title }
        case .professions:
            selectedFilter.professions = selectedFilter.professions.filter { $0 != title }
        }
        
        delegate?.didSetupFilterData()
    }
    
    func clearAll() {
        selectedFilter.ages.removeAll()
        selectedFilter.hairs.removeAll()
        selectedFilter.friends.removeAll()
        selectedFilter.professions.removeAll()
        
        delegate?.didSetupFilterData()
    }
    
    func shouldShowClear() -> Bool {
        return selectedFilter.ages.count > 0 || selectedFilter.hairs.count > 0 || selectedFilter.professions.count > 0 || selectedFilter.friends.count > 0
    }
    
    func isSelected(title: String?) -> Bool {
        guard let title = title else {
            return false
        }
        return selectedFilter.ages.contains(title) || selectedFilter.hairs.contains(title) || selectedFilter.professions.contains(title) || selectedFilter.friends.contains(title)
    }
    
    func getSelectedFilter() -> SelectedFilter? {
        if selectedFilter.ages.count == 0 && selectedFilter.friends.count == 0 && selectedFilter.hairs.count == 0 && selectedFilter.professions.count == 0 {
            return nil
        }
        return selectedFilter
    }
    
    // MARK: privates functions
    
    private func createAgeFilter() {
        ages = town.gnomes.sorted(by: { $0.0.age < $0.1.age } ).flatMap { "\($0.age)" }.removeDuplicates
    }
    
    private func createProfessionsFilter() {
        town.gnomes.forEach { gnome in gnome.professions.forEach { self.professions.append($0) } }
        professions = professions.removeDuplicates
    }
    
    private func createHairColorFilter() {
        hairs = town.gnomes.flatMap({ $0.hair_color }).removeDuplicates
    }
    
    private func createFriendsFilter() {
        town.gnomes.forEach { gnome in gnome.friends.forEach { self.friends.append($0) } }
        friends = friends.removeDuplicates
    }
    
    private func elementsForSection(section: Int) -> [String] {
        guard let section = Section(rawValue: section) else {
            return [""]
        }
        
        switch section {
        case .professions:
            return professions
        case .hairs:
            return hairs
        case .friends:
            return friends
        case .age:
            return ages
        }
    }
}

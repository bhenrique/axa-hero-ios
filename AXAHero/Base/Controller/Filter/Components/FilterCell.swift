//
//  FilterCell.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import UIKit

struct FilterCellDTO {
    var title = ""
}

class FilterCell: UITableViewCell {

    @IBOutlet weak var filterTitleLbl: UILabel!

    var title: String? {
        return filterTitleLbl.text
    }
    
    func fill(dto: FilterCellDTO) {
        filterTitleLbl.text = dto.title
    }
}

//
//  UITableViewCell+Extensions.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    class func createCell<T: UITableViewCell>(tableView: UITableView, indexPath: IndexPath) -> T {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as? T {
            return cell
        }
        return T()
    }
}

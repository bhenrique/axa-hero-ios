//
//  UIAlertViewController+Extension.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/10/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    func showTryAgainAlert(message: String, completeBlock: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Try again", style: .default, handler: completeBlock)
        let notAction = UIAlertAction(title: "Cancel", style: .default, handler: completeBlock)
        alertController.addAction(notAction)
        alertController.addAction(yesAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func startLoading() {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
}

//
//  BrastlewarkRequest.swift
//  AXAHero
//
//  Created by Bruno Henrique Machado dos Santos on 7/9/17.
//  Copyright © 2017 Bruno Santos. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONDictionary = [String: Any]

class BrastlewarkRequest {
    func getTown(completion: @escaping (_ success: Bool, _ brastlewark: Brastlewark?) -> Void) {
        Alamofire.request("https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json").responseJSON { response in
            if let _ = response.result.error {
                completion(false, nil)
                return
            }
            
            guard let json = response.result.value as? JSONDictionary else {
                completion(false, nil)
                return
            }
        
            completion(true, Brastlewark(JSON: json))
        }
    }
}
